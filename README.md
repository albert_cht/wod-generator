## Description

First of all, I chose `symfony/console` to bootstrap a console boilerplate app. `wod` file under root directory is the entry point for the application. It will require `/vendor/autoload.php` and register commands under `config/commands.php` folder automatically.

There are few folder in charge of different functioalites.

* `app/Commands`: Commands for the console.
* `app/Contracts`: Defined interfaces in this application.
* `app/Participants`: Participant related classes. Each imported participant will be encapsulated to a participant object and collected to a participant collection class.
    * `app/Participants/Importer`: Import participants from different resources, such as config file, json file, csv file, etc. (Only config file is implemented in this assignment.)
* `app/Sport`: Sport element related files, including break generator, element generator and generator rules.
    * `app/Sport/Pipes`: Rules for excluding specific sport elements.
* `tests`: Unit tests for according classes.

## Usage

```bash
php wod generate
```