<?php

return [
    [
        'name' => 'Camille',
        'beginner' => false
    ],
    [
        'name' => 'Michael',
        'beginner' => false
    ],
    [
        'name' => 'Tom',
        'beginner' => true
    ],
    [
        'name' => 'Tim',
        'beginner' => false
    ],
    [
        'name' => 'Erik',
        'beginner' => false
    ],
    [
        'name' => 'Lars',
        'beginner' => false
    ],
    [
        'name' => 'Mathijs',
        'beginner' => true
    ]
];