<?php

namespace App\Sport;

use App\Sport\Sport;
use App\Sport\BreakGenerator;
use App\Sport\ElementGenerator;
use App\Participant\Participant;

class PersonalGenerator
{
    protected $participant;

    protected $hasHandstand = false;

    protected $minutes = 30;

    protected $breakGenerator;

    protected $elementGenerator;

    public function __construct(Participant $participant, BreakGenerator $breakGenerator, ElementGenerator $elementGenerator)
    {
        $this->participant = $participant;
        $this->breakGenerator = $breakGenerator;
        $this->elementGenerator = $elementGenerator;
    }

    public function setMinutes(int $minutes): self
    {
        $this->minutes = $minutes;

        return $this;
    }

    public function setHasHandstand(bool $handstand): self
    {
        $this->hasHandstand = $handstand;

        return $this;
    }

    public function generate(): array
    {
        $minutes = array_flip(range(0, $this->minutes - 1));
        $breaks = $this->getBreaks();

        foreach ($minutes as $minute => $value) {
            if (in_array($minute, $breaks)) {
                $minutes[$minute] = [
                    'name' => 'Take a break',
                    'break' => true
                ];
                continue;
            }
            $element = $this->getElement($minute, $minutes);
            if ($this->participant->beginner && $element['name'] === Sport::HANDSTAND_NAME) {
                $this->hasHandstand = true;
            }
            $minutes[$minute] = $element;
        }

        return $minutes;
    }

    protected function getBreaks(): array
    {
        return $this->breakGenerator
            ->setMinutes($this->minutes)
            ->setTimes($this->participant->beginner ? 4 : 2)
            ->generate();
    }

    protected function getElement(int $current, array $minutes): array
    {
        return $this->elementGenerator
            ->setCurrentMinute($current)
            ->setExcludeHandstand($this->hasHandstand)
            ->setExcludeCaridos($minutes[$current - 1]['carido'] ?? false)
            ->generate();
    }
}
