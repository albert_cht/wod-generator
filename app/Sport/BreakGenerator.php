<?php

namespace App\Sport;

class BreakGenerator
{
    protected $minutes = 30;

    protected $times = 2;

    public function __construct(int $minutes = 30, int $times = 2)
    {
        $this->setMinutes($minutes);
    }

    public function setMinutes(int $minutes): self
    {
        $this->minutes = $minutes;

        return $this;
    }

    public function getMinutes(): int
    {
        return $this->minutes;
    }

    public function setTimes(int $times): self
    {
        $this->times = $times;

        return $this;
    }

    public function getTimes(): int
    {
        return $this->times;
    }

    public function generate(): array
    {
        // prefill minutes array
        $minutes = range(1, $this->minutes - 2);

        // shuffle minutes
        shuffle($minutes);

        // pop shuffled minutes
        $result = [];
        for ($i = 0; $i < $this->times; $i++) {
            if (count($minutes)) {
                $result[] = array_pop($minutes);
            }
        }

        return $result;
    }
}
