<?php

namespace App\Sport;

use App\Sport\Sport;
use App\Sport\Pipes\ExcludeRingsPipe;
use App\Sport\Pipes\ExcludeCaridosPipe;
use App\Sport\Pipes\ExcludePullUpsPipe;
use App\Sport\Pipes\ExcludeHandstandPipe;

class ElementGenerator
{
    protected $pipes = [
        ExcludeHandstandPipe::class,
        ExcludeRingsPipe::class,
        ExcludePullUpsPipe::class,
        ExcludeCaridosPipe::class,
    ];

    protected static $ringsMinutes = [];

    protected static $pullUpsMinutes = [];

    protected $excludeCaridos = false;

    protected $excludeHandstand = false;

    protected $currentMinute = 0;

    public function __construct(int $minute = 0)
    {
        $this->setCurrentMinute($minute);
    }

    public function setRingsMinutes(array $ringsMinutes): self
    {
        static::$ringsMinutes = $ringsMinutes;

        return $this;
    }

    public function getRingsMinutes(): array
    {
        return static::$ringsMinutes;
    }

    public function setPullUpsMinutes(array $pullUpsMinutes): self
    {
        static::$pullUpsMinutes = $pullUpsMinutes;

        return $this;
    }

    public function getPullUpsMinutes(): array
    {
        return static::$pullUpsMinutes;
    }

    public function setCurrentMinute(int $minute): self
    {
        $this->currentMinute = $minute;

        return $this;
    }

    public function getCurrentMinute(): int
    {
        return $this->currentMinute;
    }

    public function setExcludeHandstand(bool $exclude): self
    {
        $this->excludeHandstand = $exclude;

        return $this;
    }

    public function getExcludeHandstand(): bool
    {
        return $this->excludeHandstand;
    }

    public function setExcludeCaridos(bool $exclude): self
    {
        $this->excludeCaridos = $exclude;

        return $this;
    }

    public function getExcludeCaridos(): bool
    {
        return $this->excludeCaridos;
    }

    public function getElements(): array
    {
        $elements = Sport::ELEMENTS;
        foreach ($this->pipes as $class) {
            $elements = (new $class)->handle($this, $elements);
        }

        return $elements;
    }

    public function generate(): array
    {
        $elementKey = array_rand($this->getElements(), 1);
        if ($elementKey === SPORT::RINGS) {
            static::$ringsMinutes[] = $this->currentMinute;
        } elseif ($elementKey === SPORT::PULL_UPS) {
            static::$pullUpsMinutes[] = $this->currentMinute;
        }

        return Sport::ELEMENTS[$elementKey];
    }
}
