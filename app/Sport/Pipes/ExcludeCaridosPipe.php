<?php

namespace App\Sport\Pipes;

use App\Sport\ElementGenerator;
use App\Contracts\ElementPipeContract;

class ExcludeCaridosPipe implements ElementPipeContract
{
    public function handle(ElementGenerator $generator, array $elements): array
    {
        if ($generator->getExcludeCaridos()) {
            return array_filter($elements, function ($element) {
                return ! $element['carido'];
            });
        }

        return $elements;
    }
}
