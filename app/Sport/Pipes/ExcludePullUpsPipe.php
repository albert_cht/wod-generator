<?php

namespace App\Sport\Pipes;

use App\Sport\ElementGenerator;
use App\Contracts\ElementPipeContract;

class ExcludePullUpsPipe implements ElementPipeContract
{
    public function handle(ElementGenerator $generator, array $elements): array
    {
        if (in_array($generator->getCurrentMinute(), $generator->getPullUpsMinutes())) {
            unset($elements['pull_ups']);
        }

        return $elements;
    }
}
