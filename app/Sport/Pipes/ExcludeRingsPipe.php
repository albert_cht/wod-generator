<?php

namespace App\Sport\Pipes;

use App\Sport\ElementGenerator;
use App\Contracts\ElementPipeContract;

class ExcludeRingsPipe implements ElementPipeContract
{
    public function handle(ElementGenerator $generator, array $elements): array
    {
        if (in_array($generator->getCurrentMinute(), $generator->getRingsMinutes())) {
            unset($elements['rings']);
        }

        return $elements;
    }
}
