<?php

namespace App\Sport\Pipes;

use App\Sport\ElementGenerator;
use App\Contracts\ElementPipeContract;

class ExcludeHandstandPipe implements ElementPipeContract
{
    public function handle(ElementGenerator $generator, array $elements): array
    {
        if ($generator->getExcludeHandstand()) {
            unset($elements['handstand']);
        }

        return $elements;
    }
}
