<?php

namespace App\Sport;

class Sport
{
    public const ELEMENTS = [
        'jumping_jacks' => [
            'name' => 'Jumping Jacks',
            'carido' => true
        ],
        'push_ups' => [
            'name' => 'Push Ups',
            'carido' => false
        ],
        'front_squats' => [
            'name' => 'Front Squats',
            'carido' => false
        ],
        'back_squats' => [
            'name' => 'Back Squats',
            'carido' => false
        ],
        'pull_ups' => [
            'name' => 'Pull Ups',
            'carido' => false
        ],
        'rings' => [
            'name' => 'Rings',
            'carido' => false
        ],
        'short_sprints' => [
            'name' => 'Short Sprints',
            'carido' => true
        ],
        'handstand' => [
            'name' => 'Handstand Practice',
            'carido' => false
        ],
        'jumping_rope' => [
            'name' => 'Jumping Rope',
            'carido' => true
        ]
    ];

    public const PULL_UPS = 'pull_ups';

    public const RINGS = 'rings';

    public const HANDSTAND_NAME = 'Handstand Practice';
}
