<?php

namespace App\Traits;

trait HasFilePath
{
    protected $filePath;

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }
}
