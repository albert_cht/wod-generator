<?php

namespace App\Commands;

use App\Sport\BreakGenerator;
use App\Sport\ElementGenerator;
use App\Sport\PersonalGenerator;
use App\Contracts\FileImportable;
use App\Contracts\ImporterContract;
use App\Participant\ParticipantCollection;
use App\Participant\Importer\ConfigImporter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCommand extends Command
{
    protected static $defaultName = 'generate';

    protected $minutes = 30;

    protected function configure()
    {
        $this->setDescription('Generate WODs for participants.')
            ->addOption(
                'minutes',
                'm',
                InputOption::VALUE_OPTIONAL,
                'Total minutes for the excercise'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Importing participants...');

        $participants = $this->getParticipants(new ConfigImporter);

        $output->writeln('Imported done!');

        $minutes = $input->getOption('minutes') ?: 30;

        $output->writeln('Generating schedules...');

        $schedules = $this->generateSchedules($participants, $minutes);

        for ($i = 0; $i < $minutes; $i++) {
            $message = '';
            $nextMinute = $i+1;
            $message .= "{$i}:00 - {$nextMinute}:00 - ";
            foreach ($schedules as $key => $schedule) {
                $element = strtolower($schedule['schedule'][$i]['name']);
                $name = $schedule['participant']->name;
                if ($schedule['participant']->beginner) {
                    $name .= ' (Beginner)';
                }
                $message .= "$name will do {$element}";
                if ($key !== count($schedules) - 1) {
                    $message .= ', ';
                }
            }

            $output->writeln($message);
        }
    }

    public function generateSchedules(ParticipantCollection $participants, int $minutes = 30): array
    {
        $schedules = [];
        foreach ($participants as $participant) {
            $schedules[] = [
                'participant' => $participant,
                'schedule' => (new PersonalGenerator(
                    $participant,
                    new BreakGenerator,
                    new ElementGenerator
                ))->setMinutes($minutes)
                ->generate()
            ];
        }

        return $schedules;
    }

    public function getParticipants(ImporterContract $importer): ParticipantCollection
    {
        if ($importer instanceOf FileImportable) {
            $importer->setFilePath(__DIR__ . '../../../config/participants.php');
        }

        $importer->import();

        return $importer->getParticipants();
    }
}
