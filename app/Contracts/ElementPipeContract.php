<?php

namespace App\Contracts;

use App\Sport\ElementGenerator;

interface ElementPipeContract
{
    public function handle(ElementGenerator $generator, array $elements): array;
}
