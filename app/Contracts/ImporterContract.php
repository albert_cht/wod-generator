<?php

namespace App\Contracts;

use App\Participant\ParticipantCollection;

interface ImporterContract
{
    public function import(): void;

    public function getResource();

    public function getParticipants(): ParticipantCollection;
}
