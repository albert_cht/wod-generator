<?php

namespace App\Participant;

use App\Contracts\Arrayable;

class Participant implements Arrayable
{
    public $name;

    public $beginner;

    public function __construct(string $name, bool $beginner = false)
    {
        $this->name = $name;
        $this->beginner = $beginner;
    }

    public function toArray(): array
    {
        return [
            'name' => $this->name,
            'beginner' => $this->beginner
        ];
    }
}
