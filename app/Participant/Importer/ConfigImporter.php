<?php

namespace App\Participant\Importer;

use App\Traits\HasFilePath;
use App\Participant\Participant;
use App\Contracts\FileImportable;
use App\Contracts\ImporterContract;
use App\Participant\ParticipantCollection;

class ConfigImporter implements ImporterContract, FileImportable
{
    use HasFilePath;

    protected $resource = [];
    protected $participants;

    public function import(): void
    {
        $this->resource = require $this->getFilePath();
    }

    public function getResource(): array
    {
        return $this->resource;
    }

    public function getParticipants(): ParticipantCollection
    {
        // cache transformed participants
        if ($this->participants instanceOf ParticipantCollection) {
            return $this->participants;
        }

        $this->participants = new ParticipantCollection;

        foreach ($this->resource as $array) {
            $this->participants->add(
                new Participant(
                    $array['name'] ?? null,
                    $array['beginner'] ?? null
                )
            );
        }

        return $this->participants;
    }
}
