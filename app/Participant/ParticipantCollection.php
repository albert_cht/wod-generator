<?php

namespace App\Participant;

use Countable;
use ArrayIterator;
use IteratorAggregate;

class ParticipantCollection implements IteratorAggregate, Countable
{
    protected $participants = [];

    public function add(Participant $participant): self
    {
        $this->participants[] = $participant;

        return $this;
    }

    public function count(): int
    {
        return count($this->participants);
    }

    public function get(): array
    {
        return $this->participants;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->participants);
    }

    public function toArray(): array
    {
        return array_map(function ($value) {
            return $value->toArray();
        }, $this->participants);
    }
}
