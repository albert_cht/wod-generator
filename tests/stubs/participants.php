<?php

return [
    [
        'name' => 'Foo',
        'beginner' => false
    ],
    [
        'name' => 'Bar',
        'beginner' => false
    ],
    [
        'name' => 'Hello',
        'beginner' => true
    ],
    [
        'name' => 'World',
        'beginner' => true
    ]
];
