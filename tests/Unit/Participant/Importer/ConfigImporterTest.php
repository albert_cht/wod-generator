<?php

namespace Tests\Unit\Participant\Importer;

use PHPUnit\Framework\TestCase;
use App\Participant\Participant;
use App\Participant\ParticipantCollection;
use App\Participant\Importer\ConfigImporter;

class ConfigImporterTest extends TestCase
{
    public function testImport(): void
    {
        $filePath = __DIR__ . '../../../../stubs/participants.php';
        $importer = (new ConfigImporter)->setFilePath($filePath);
        $importer->import();

        $this->assertEquals(
            require $filePath,
            $importer->getResource()
        );
    }

    public function testGetParticipants(): void
    {
        $filePath = __DIR__ . '../../../../stubs/participants.php';
        $importer = (new ConfigImporter)->setFilePath($filePath);
        $importer->import();

        $resources = $importer->getResource();
        $participants = $importer->getParticipants();

        $this->assertInstanceOf(ParticipantCollection::class, $participants);
        $this->assertSame(
            count($resources),
            count($participants)
        );

        $firstParticipant = new Participant(
            $resources[0]['name'] ?? null,
            $resources[0]['beginner'] ?? null
        );

        $this->assertEquals(
            (array) $firstParticipant,
            $participants->toArray()[0]
        );
    }
}
