<?php

namespace Tests\Unit\Participant;

use PHPUnit\Framework\TestCase;
use App\Participant\Participant;
use App\Participant\ParticipantCollection;

class ParticipantCollectionTest extends TestCase
{
    public function testAdd(): void
    {
        $collection = new ParticipantCollection;
        $collection->add($participantA = new Participant('Jhon'));
        $collection->add($participantB = new Participant('Doe'));

        $this->assertSame(
            [
                $participantA,
                $participantB
            ]
            , $collection->get()
        );
    }

    public function testCount(): void
    {
        $collection = new ParticipantCollection;
        $collection->add($participantA = new Participant('Jhon'));
        $collection->add($participantB = new Participant('Doe'));

        $this->assertSame(2, $collection->count());
    }

    public function testToArray(): void
    {
        $collection = new ParticipantCollection;
        $collection->add($participantA = new Participant('Jhon'));
        $collection->add($participantB = new Participant('Doe'));

        $this->assertEquals(
            [
                $participantA,
                $participantB
            ],
            (array) $collection->get()
        );
    }
}
