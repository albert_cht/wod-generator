<?php

namespace Tests\Unit\Participant;

use PHPUnit\Framework\TestCase;
use App\Participant\Participant;

class ParticipantTest extends TestCase
{
    public function testConstructor(): void
    {
        $participant = new Participant($name = 'Jhon');

        $this->assertSame($name, $participant->name);
        $this->assertFalse($participant->beginner);
    }

    public function testToArray(): void
    {
        $participant = new Participant($name = 'Jhon', $beginner = true);

        $this->assertSame(
            [
                'name' => $name,
                'beginner' => $beginner
            ],
            (array) $participant
        );
    }
}
