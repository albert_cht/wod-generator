<?php

namespace Tests\Unit\Sport\Pipes;

use App\Sport\Sport;
use App\Sport\ElementGenerator;
use PHPUnit\Framework\TestCase;
use App\Sport\Pipes\ExcludePullUpsPipe;

class ExcludePullUpsTest extends TestCase
{
    public function testHandleWithMissingPullUps(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setPullUpsMinutes([1]);
        $result = (new ExcludePullUpsPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertFalse(
            array_key_exists('pull_ups', $result)
        );
    }

    public function testHandleWithKeepingPullUps(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setPullUpsMinutes([2]);
        $result = (new ExcludePullUpsPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertTrue(
            array_key_exists('pull_ups', $result)
        );
    }
}
