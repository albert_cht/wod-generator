<?php

namespace Tests\Unit\Sport\Pipes;

use App\Sport\Sport;
use App\Sport\ElementGenerator;
use PHPUnit\Framework\TestCase;
use App\Sport\Pipes\ExcludeCaridosPipe;

class ExcludeCaridosTest extends TestCase
{
    public function testHandleWithMissingCaridos(): void
    {
        $generator = (new ElementGenerator)
            ->setExcludeCaridos(true);
        $result = (new ExcludeCaridosPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertFalse(
            array_key_exists('jumping_jacks', $result)
        );

        $this->assertFalse(
            array_key_exists('short_sprints', $result)
        );

        $this->assertFalse(
            array_key_exists('jumping_rope', $result)
        );
    }

    public function testHandleWithKeepingCaridos(): void
    {
        $generator = (new ElementGenerator)
            ->setExcludeCaridos(false);
        $result = (new ExcludeCaridosPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertEquals(
            $result,
            Sport::ELEMENTS
        );
    }
}
