<?php

namespace Tests\Unit\Sport\Pipes;

use App\Sport\Sport;
use App\Sport\ElementGenerator;
use PHPUnit\Framework\TestCase;
use App\Sport\Pipes\ExcludeRingsPipe;

class ExcludeRingsTest extends TestCase
{
    public function testHandleWithMissingRings(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setRingsMinutes([1]);
        $result = (new ExcludeRingsPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertFalse(
            array_key_exists('rings', $result)
        );
    }

    public function testHandleWithKeepingRings(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setRingsMinutes([2]);
        $result = (new ExcludeRingsPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertTrue(
            array_key_exists('rings', $result)
        );
    }
}
