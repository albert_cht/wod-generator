<?php

namespace Tests\Unit\Sport\Pipes;

use App\Sport\Sport;
use App\Sport\ElementGenerator;
use PHPUnit\Framework\TestCase;
use App\Sport\Pipes\ExcludeHandstandPipe;

class ExcludeHandstandTest extends TestCase
{
    public function testHandleWithMissingHandstand(): void
    {
        $generator = (new ElementGenerator)
            ->setExcludeHandstand(true);
        $result = (new ExcludeHandstandPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertFalse(
            array_key_exists('handstand', $result)
        );
    }

    public function testHandleWithKeepingHandstand(): void
    {
        $generator = (new ElementGenerator)
            ->setExcludeHandstand(false);
        $result = (new ExcludeHandstandPipe)
            ->handle($generator, Sport::ELEMENTS);

        $this->assertTrue(
            array_key_exists('handstand', $result)
        );
    }
}
