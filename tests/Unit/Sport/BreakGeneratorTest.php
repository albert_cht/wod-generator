<?php

namespace Tests\Unit\Sport;

use App\Sport\BreakGenerator;
use PHPUnit\Framework\TestCase;

class BreakGeneratorTest extends TestCase
{
    public function testGenerate(): void
    {
        $result = (new BreakGenerator)
            ->setMinutes(4)
            ->setTimes(2)
            ->generate();

        $expected = [1, 2];
        $this->assertSame(0, count(array_diff($expected, $result)));
        $this->assertSame(0, count(array_diff($result, $expected)));
    }
}
