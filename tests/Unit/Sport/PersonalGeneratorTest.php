<?php

namespace Tests\Unit\Sport;

use Mockery as m;
use App\Sport\BreakGenerator;
use App\Sport\ElementGenerator;
use PHPUnit\Framework\TestCase;
use App\Participant\Participant;
use App\Sport\PersonalGenerator;

class PersonalGeneratorTest extends TestCase
{
    public function testGenerate(): void
    {
        $breakGenerator = m::mock(BreakGenerator::class);
        $breakGenerator->shouldReceive('setMinutes')
            ->with($minutes = 30)
            ->once()
            ->andReturnSelf()
            ->shouldReceive('setTimes')
            ->with(4)
            ->andReturnSelf()
            ->shouldReceive('generate')
            ->andReturn([5, 13, 20, 25]);

        $elementGenerator = m::mock(ElementGenerator::class);
        $elementGenerator->shouldReceive('setCurrentMinute')
            ->times($minutes)
            ->andReturnSelf()
            ->shouldReceive('setExcludeHandstand')
            ->times($minutes)
            ->andReturnSelf()
            ->shouldReceive('setExcludeCaridos')
            ->times($minutes)
            ->andReturnSelf()
            ->shouldReceive('generate')
            ->times($minutes)
            ->andReturn([
                'name' => 'Mock Sport',
                'carido' => false
            ]);

        $participant = m::mock(Participant::class);
        $participant->beginner = true;

        $personalGenerator = new PersonalGenerator($participant, $breakGenerator, $elementGenerator);
        $result = $personalGenerator
            ->setMinutes($minutes)
            ->generate();

        $this->assertSame($minutes, count($result));
        $this->assertTrue($result[5]['break'] ?? false);
        $this->assertTrue($result[13]['break'] ?? false);
        $this->assertTrue($result[20]['break'] ?? false);
        $this->assertTrue($result[25]['break'] ?? false);
    }
}
