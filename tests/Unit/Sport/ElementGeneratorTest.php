<?php

namespace Tests\Unit\Sport;

use App\Sport\Sport;
use App\Sport\ElementGenerator;
use PHPUnit\Framework\TestCase;
use App\Sport\Pipes\ExcludeHandstandPipe;

class ElementGeneratorTest extends TestCase
{
    public function testGetElementsWithMissing(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setRingsMinutes([1])
            ->setPullUpsMinutes([1])
            ->setExcludeHandstand(true)
            ->setExcludeCaridos(true);

        $elements = $generator->getElements();

        $this->assertSame(3, count($elements));

        $this->assertTrue(
            array_key_exists('push_ups', $elements)
        );

        $this->assertTrue(
            array_key_exists('front_squats', $elements)
        );

        $this->assertTrue(
            array_key_exists('back_squats', $elements)
        );
    }

    public function testHandleWithKeepingHandstand(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setRingsMinutes([2])
            ->setPullUpsMinutes([2])
            ->setExcludeHandstand(false)
            ->setExcludeCaridos(false);

        $this->assertSame(
            count($generator->getElements()),
            count(Sport::ELEMENTS)
        );

        $this->assertEquals(
            $generator->getElements(),
            Sport::ELEMENTS
        );
    }

    public function testGenerate(): void
    {
        $generator = (new ElementGenerator)
            ->setCurrentMinute(1)
            ->setRingsMinutes([1])
            ->setPullUpsMinutes([1])
            ->setExcludeHandstand(true)
            ->setExcludeCaridos(true);

        $availableNames = ['Push Ups', 'Front Squats', 'Back Squats'];

        $this->assertTrue(
            in_array($generator->generate()['name'], $availableNames)
        );
    }
}
